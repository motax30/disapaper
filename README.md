# Disapaper

Web Bot tendo como finalidade a pesquisa de artigos tecnológicos acadêmicos visando um melhor direcionamento para o desenvolvimento de novos artigos, nos quais, tais informações servirão de base para referências entre outras funcionalidades. 

### Passos para subir a aplicação back-end

Deve se acessar a pasta: disapaper/backend

Para subir a aplicação devemos executar o seguinte comando: python manage.py runserver

Obs: Não rodar com rede da FATEC, pois vai dar um block na Migration do MongoDB

### Passos para subir o front-end.

----------------------------------------------------------------------------

#### Dependências

Antes de subir a aplicação é necessário que tenha alguns itens instalados no seu computador:

* [Nodejs](https://nodejs.org/en/download/)
* [NPM](https://www.npmjs.com/)

**Ao instalar o nodejs o NPM já é instalado também**

Após feita a instalação verifique se está tudo certo com esses comandos:

**Nodejs**
```bash
node --version
```
A saída para esse comando deve mais ou menos essa: (Não tem problema se a versão estiver diferente) ```v10.16.0```

**NPM**
```bash
node --version
```
A saída para esse comando deve mais ou menos essa: (Não tem problema se a versão estiver diferente) ```6.9.0```

#### Subindo a aplicação

1. Primeiro de tudo é necessário abrir seu cmd ou gitbash e entrar na pasta do frontend do disapaper com o comando 'cd'. Exemplo:<br />
    ```cd <caminho para a pasta do disapaper>/frontend```
2. Após feito isso, é necessário instalar as dependências do projeto, para isso execute o seguinte comando:<br />
    ```npm install```
3. Se tudo deu certo até aqui então estamos prontos para rodar a aplicação, para isso execute:<br />
    ```npm run serve```
4. Nesse momento o serviço já está rodando, é só ir no navegador e colocar o link que é mostrado no terminal que é parecido com isso: ```http://localhost:8080```.<br />
**Obs: A porta pode variar, então se atente a isso quando for rodar o projeto.**
















