import axios, { AxiosInstance } from "axios";

export default class Service {
  public apiURL: string = "";
  public client: AxiosInstance;

  public constructor() {
    // this.apiURL = "http://localhost:5000/";
    this.apiURL = "http://142.93.49.99:5000/";
    this.client = axios.create({
      baseURL: this.apiURL,
    });
    this.client.defaults.headers["Access-Control-Allow-Origin"] = "*";
    this.client.defaults.headers["Content-Type"] = "application/json";
  }

  public async getArticles(query: string) {
    const r = await this.client.get(`articles?query=${query}`);
    return r.data;
  }

  public async scrapSbc(query: string) {
    const r = await this.client.get(`scraper/sbc?query=${query}`);
    return r.data;
  }
}
