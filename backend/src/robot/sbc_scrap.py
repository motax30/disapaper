from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.error import URLError
import requests
import lxml
from lxml import html


def scrapSbcSearch(searchword):
    searchword = searchword.replace(" ", "%20")
    z = []
    url = (f'http://www.sbc.org.br/component/search/?searchword={searchword}&ordering=newest&searchphrase=all&limit=0')
    html = requests.get(url).text
    bs = BeautifulSoup(html, 'html.parser')
    resultados = bs.findAll('a', href = True)
    resultados1 = bs.findAll('dt', {'class':'result-title'})
    numLi = 0
    for li in resultados[67:-9]:
        artigo = {}
        if("publicacoes" in li['href']):
            titulo = li.text.replace('\n','').replace('\t','')
            link ='http://sbc.org.br'+li['href']
            res = requests.get(link, headers={'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'})
            doc = lxml.html.fromstring(res.content)
            resumo = doc.xpath('//*[@id="t3-content"]/div/article/section/p[1]/text()[2]')
            html2 = requests.get(link).text
            bs2 = BeautifulSoup(html2, 'html.parser')
            artigo.update({"link": link, "titulo": titulo, "resumo": resumo[0]})
            z.append(artigo)
    return z