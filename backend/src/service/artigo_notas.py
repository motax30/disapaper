import _datetime
# ##from artigo  import Artigo
artigo = {'titulo': str, 'author': [], 'escola': str,
          'temas': [], 'link': str, 'fonte': str, 'qtdcitacoes': int}
estrelas = str


def adicionaLista(artigo):
    artigo['qtdcitacoes'] = 0

# Incrementa a Qtdcitações do artigo


def nota(artigo):
    artigo['qtdcitacoes'] += 1

# Classifica o artigo, podendo ele ser em 1,2,3,4 e 5 estrelas


def classificacao(artigo):
    if artigo['qtdcitacoes'] < 200:
        return '*1,' + 'citações='+str(artigo['qtdcitacoes'])
    elif artigo['qtdcitacoes'] >= 200 and artigo['qtdcitacoes'] < 400:
        return '*2,' + 'citações='+str(artigo['qtdcitacoes'])
    elif artigo['qtdcitacoes'] >= 400 and artigo['qtdcitacoes'] < 600:
        return '*3,' + 'citações='+str(artigo['qtdcitacoes'])
    elif artigo['qtdcitacoes'] >= 600 and artigo['qtdcitacoes'] < 800:
        return '*4,' + 'citações='+str(artigo['qtdcitacoes'])
    elif artigo['qtdcitacoes'] >= 800:
        return '*5,' + 'citações='+str(artigo['qtdcitacoes'])
