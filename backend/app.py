from flask import Flask, request, jsonify
from src.service.articles import find_article
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/")
def hello_world():
  return "Server is running! 😍"

@app.route("/articles", methods=["GET"])
@cross_origin()
def get_articles():
  query = request.args.get("query")
  return jsonify(find_article(query))

if __name__ == "__main__":
  app.run(host="0.0.0.0", port="5000", debug=True)